Role Name
=========

This role download all the required BPM binaries from the following azure blob directories:
1. BPM
2. IM
3. WAS
4. Misc

Requirements
------------

Binaries have been uploaded to the storage location listed above in the azure blob

Role Variables
--------------
Default Variables:
azure_dir: "/software/azure"

azure_bpm_app_packages:
  - { name: "BPM", storage_location: "https://staprodsgrhobaseimages.file.core.windows.net/storage/BPM", path: "/software/BPM"}
  - { name: "IM", storage_location: "https://staprodsgrhobaseimages.file.core.windows.net/storage/IM", path: "/software/IM"}
  - { name: "WAS", storage_location: "https://staprodsgrhobaseimages.file.core.windows.net/storage/WAS", path: "/software/WAS"}
  - { name: "Misc", storage_location: "https://staprodsgrhobaseimages.file.core.windows.net/storage/Misc", path: "/software/Misc"}

azure_source_key: "EoT3ZyFpckN4Vse0iUDF3i0HyNFjnUr4GDzpxoK7f08ez7c9NZdd5gjUIgMghrc/0iF+tuO/pNNm5AvLzQTU3w=="


Dependencies
------------

A list of other roles hosted on Galaxy should go here, plus any details in regards to parameters that may need to be set for other roles, or variables that are used from other roles.

Example Playbook
----------------

Including an example of how to use your role (for instance, with variables passed in as parameters) is always nice for users too:

    - hosts: servers
      roles:
         - { role: username.rolename, x: 42 }

License
-------

BSD

Author Information
------------------

An optional section for the role authors to include contact information, or a website (HTML is not allowed).
